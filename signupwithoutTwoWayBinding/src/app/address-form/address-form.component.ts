import { Component, OnInit , Input, Output, AfterViewInit , OnChanges, SimpleChanges} from '@angular/core';
import { NgForm, FormsModule, FormGroup } from '@angular/forms';
// import { Billingaddress } from '../billingaddress';
import { Address } from '../address';
import { ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';




@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements  AfterViewInit, OnInit , OnChanges {

 isSubmitted = true ;

@Output() address = new EventEmitter<Address>();
@Input() addressObj: Address;
@Input() update: boolean ;

@Input() addressFormReset: boolean ;

 @ViewChild('addressForm', {static: false} ) formContent: NgForm;

  reset(addressForm: NgForm) {
    addressForm.form.reset();

 }
  constructor() { }



  ngOnInit() {  }


  ngAfterViewInit()  {
    this.formContent.form.valueChanges.subscribe(() => {

       this.addressObj.line1 = this.formContent.form.getRawValue().line1;
       this.addressObj.line2 = this.formContent.form.getRawValue().line2;
       this.addressObj.city =  this.formContent.form.getRawValue().city;
       this.addressObj.state = this.formContent.form.getRawValue().state;
       this.addressObj.country = this.formContent.form.getRawValue().country;
       this.addressObj.zip = this.formContent.form.getRawValue().zip;
       this.address.emit(this.addressObj);

      });
      }

  ngOnChanges(changes: SimpleChanges) {

      this.formContent.form.setValue({
        line1 : this.addressObj.line1,
        line2 : this.addressObj.line2,
        city: this.addressObj.city,
        state: this.addressObj.state,
        country: this.addressObj.country,
        zip: this.addressObj.zip
      });
      console.log('addressObj after', this.addressObj);


  }



  }








